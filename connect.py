import pymysql
import csv

allin= open("creds.txt", "r").read().split("\n")    
creds = {"cloud":allin[0]}

def getcsv():
    csvfile = list(csv.reader(open("data.csv") ) )
    formatted = tuple( [tuple(column) for column in csvfile] )
    return formatted
    

def connect():
    try:
        connection = pymysql.connect(host='35.193.83.184', user=creds['cloud'], password=creds['cloud'], db='ELAData')
        with connection.cursor() as cursor:
            print("We can connect to Cloud SQL")
            cursor.execute("DROP TABLE IF EXISTS ELA")

            #change these two if you need to add more columns to your SQL table
            cursor.execute('''CREATE TABLE ELA(First_Last_Name varchar(255),
                                        ELA_Email varchar(255), 
                                        Program varchar(255), 
                                        Track varchar(255), 
                                        Location varchar(255))''')
            cursor.executemany("INSERT INTO ELA(First_Last_Name, ELA_Email,Program,Track,Location) VALUES (%s, %s, %s, %s, %s)", getcsv())

            # commit it to the database
            connection.commit()
            print("Done")

    except Exception as e:
        print("Something went wrong:\n{}".format(e))
        return []

if __name__=="__main__":
    connect()